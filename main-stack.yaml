AWSTemplateFormatVersion: "2010-09-09" #Can only be this value

Description: AWS CloudFormation - Nested stacks - Root template. #Describes the contents of the Template 

#Metadata: If you want to rearrange/ organize how the parameters are displayed in the console to make deployment more user friendly

Parameters: #This is what show up in the first page after uploading the template in the console 
  EnvironmentType:
    Description: 'Specify the Environment type of the stack.'
    Type: String
    Default: Test
    AllowedValues:
      - Dev
      - Test
      - Prod
    ConstraintDescription: 'Specify either Dev, Test or Prod.'

  S3BucketName:
    AllowedPattern: ^[0-9a-zA-Z]+([0-9a-zA-Z-.]*[0-9a-zA-Z])*$
    ConstraintDescription: Bucket name can include numbers, lowercase letters, uppercase letters, periods (.), and hyphens (-). It cannot start or end with a hyphen (-).
    Description: S3 bucket name for the Nested Stacks. S3 bucket name can include numbers, lowercase letters, uppercase letters, and hyphens (-). It cannot start or end with a hyphen (-).
    Type: String

  AvailabilityZones:
    Type: List<AWS::EC2::AvailabilityZone::Name>
    Description: The list of Availability Zones to use for the subnets in the VPC. Select 2 AZs.

  VPCName:
    Type: String
    Description: The name of the VPC.
    Default: project3-vpc-stack 

  VPCCIDR:
    Type: String
    Description: The CIDR block for the VPC.
    AllowedPattern: ^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|[1-2][0-9]|3[0-2]))$
    ConstraintDescription: CIDR block parameter must be in the form x.x.x.x/16-28
    Default: 10.0.0.0/16

  PublicSubnet1CIDR:
    Type: String
    Description: The CIDR block for the public subnet located in Availability Zone 1.
    AllowedPattern: ^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|[1-2][0-9]|3[0-2]))$
    ConstraintDescription: CIDR block parameter must be in the form x.x.x.x/16-28
    Default: 10.0.0.0/24

  PublicSubnet2CIDR:
    Type: String
    Description: The CIDR block for the public subnet located in Availability Zone 2.
    AllowedPattern: ^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|[1-2][0-9]|3[0-2]))$
    ConstraintDescription: CIDR block parameter must be in the form x.x.x.x/16-28
    Default: 10.0.1.0/24

  PrivateSubnet1CIDR:
    Type: String
    Description: The CIDR block for the private subnet located in Availability Zone 1.
    AllowedPattern: ^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|[1-2][0-9]|3[0-2]))$
    ConstraintDescription: CIDR block parameter must be in the form x.x.x.x/16-28
    Default: 10.0.2.0/24

  PrivateSubnet2CIDR:
    Type: String
    Description: The CIDR block for the private subnet located in Availability Zone 2.
    AllowedPattern: ^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|[1-2][0-9]|3[0-2]))$
    ConstraintDescription: CIDR block parameter must be in the form x.x.x.x/16-28
    Default: 10.0.3.0/24


Resources:
  VpcStack:
    Type: AWS::CloudFormation::Stack
    Properties:
      TemplateURL: !Sub https://${S3BucketName}.s3.amazonaws.com/vpc-stack.yaml #(The S3 bucket where your tempalates are stored... make sure to change the name of the yaml file to the right associated file)
      TimeoutInMinutes: 20
      Parameters: #(Must reference the parameters that are in the associated stack template i.e. VPC template)
        AvailabilityZones:
          Fn::Join:
            - ','
            - !Ref AvailabilityZones #(referencing the parameters in the vpc template)
        VPCCIDR: !Ref VPCCIDR #(referencing the parameters in the vpc template)
        VPCName: !Ref VPCName #(referencing the parameters in the vpc template)
        
  PublicSubnetStack:
    Type: AWS::CloudFormation::Stack
    Properties:
      TemplateURL: !Sub https://${S3BucketName}.s3.amazonaws.com/publicsubnet-stack.yaml
      TimeoutInMinutes: 20
      Parameters:
        AvailabilityZones:
          Fn::Join:
            - ','
            - !Ref AvailabilityZones
        VpcID: !GetAtt VpcStack.Outputs.VpcID #(to reference the VpcID must get the output of the VpcStack)
          #explaination- !Get Att <NameOfTheStackInRootTemp>.<Outputs>.<Name of the output in the template you're referencing (VPC template) *Not the output value but output name>
        VPCName: !Ref VPCName
        PublicSubnet1CIDR: !Ref PublicSubnet1CIDR
        PublicSubnet2CIDR: !Ref PublicSubnet2CIDR
        
  PrivateSubnetStack:
    Type: AWS::CloudFormation::Stack
    Properties:
      TemplateURL: !Sub https://${S3BucketName}.s3.amazonaws.com/privatesubnet-stack.yaml
      TimeoutInMinutes: 20
      Parameters:
        AvailabilityZones:
          Fn::Join:
            - ','
            - !Ref AvailabilityZones
        VpcID: !GetAtt VpcStack.Outputs.VpcID
        VPCName: !Ref VPCName
        PrivateSubnet1CIDR: !Ref PrivateSubnet1CIDR
        PrivateSubnet2CIDR: !Ref PrivateSubnet2CIDR
          
  InternetGatewayPublicRouteTablesStack:
    Type: AWS::CloudFormation::Stack
    Properties:
      TemplateURL: !Sub https://${S3BucketName}.s3.amazonaws.com/igw-publicrt-stack.yaml
      TimeoutInMinutes: 20
      Parameters:
        VPCName: !Ref VPCName
        VpcID: !GetAtt VpcStack.Outputs.VpcID
        PublicSubnet1: !GetAtt PublicSubnetStack.Outputs.PublicSubnet1
        PublicSubnet2: !GetAtt PublicSubnetStack.Outputs.PublicSubnet2

  NATGatewayPrivateRouteTableStack:
    Type: AWS::CloudFormation::Stack
    Properties:
      TemplateURL: !Sub https://${S3BucketName}.s3.amazonaws.com/nat-privatert-stack.yaml
      TimeoutInMinutes: 20
      Parameters:
        VPCName: !Ref VPCName
        VpcID: !GetAtt VpcStack.Outputs.VpcID
        PublicSubnet1: !GetAtt PublicSubnetStack.Outputs.PublicSubnet1
        PublicSubnet2: !GetAtt PublicSubnetStack.Outputs.PublicSubnet2
        PrivateSubnet1: !GetAtt PrivateSubnetStack.Outputs.PrivateSubnet1
        PrivateSubnet2: !GetAtt PrivateSubnetStack.Outputs.PrivateSubnet2

  SecurityGroups:
    Type: AWS::CloudFormation::Stack
    Properties:
      TemplateURL: !Sub https://${S3BucketName}.s3.amazonaws.com/sg-stack.yaml
      Parameters: 
        EnvironmentType: !Ref EnvironmentType
        VpcID: !GetAtt VpcStack.Outputs.VpcID