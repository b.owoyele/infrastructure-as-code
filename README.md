## **Objectives:**

1. Create and deploy a CloudFormation stack that will launch the following resources:
   * VPC
   * Internet Gateway
   * 2 Public Subnets (in different AZs)
   * 2 Private Subnets (in different AZs)
   * 2 NAT Gateways
   * Public Route Table
   * 2 Private Route Tables
   * Ingress Security Group
2. Once you deploy successfully in the console, deploy the template via the AWS CLI.
3. Once you deploy successfully in the CLI, modify the same template to deploy as a nested stack in the console.
4. Once you deploy successfully in the console, deploy the nested stack via the AWS CLI.

## **Architecture:**

![Architecture](VPC.drawio.png)

## **Solutions:**

#### **Objective 1:**

1. Open Terminal on your computer
2. Go to your desktop (or the home directory in which you want to create the directory where you will store your template using the following command: **cd desktop**
3. Create a directory where your templates will be stored using the following command: **mkdir directoryname**
4. Create a yaml file using the following command: **touch <filename.yaml>** (it is within this file that you will construct your CloudFormation template)
5. Open the yaml file and create your cloud formation template
   * A CloudFormation template contains the following categories (not all are required)
      1. AWSTemplateFormatVersion (optional)
      2. Description (optional)
      3. Metadata (optional)
      4. Parameters (optional)
      5. Rules (optional)
      6. Mappings (optional)
      7. Conditions (optional
      8. Transform (optional)
      9. Resources (required)
     10. Outputs (optional)
   * See main-nonstack in the repository as an example template
    
6. To launch your template in the AWS Console but performing the following steps:
   1. Login to your AWS Console
   2. Go to the CloudFormation dashboard
   3. Choose create stack
   4. Upload stack under the following settings

      <details>
      <summary>Click Here</summary>

      ![Create_stack](https://gitlab.com/b.owoyele/images/-/raw/main/Create_stack.jpg)

      </details>
   5. Name your stack, fill-in necessary parameters, and click next until you are able to launch your stack
   6. You should see the following upon successfully launching stack

      <details>
      <summary>Click Here</summary>

      ![E_Stacks__1_](https://gitlab.com/b.owoyele/images/-/raw/main/E_Stacks__1_.jpg)

      </details>

#### **Objective 2:**

Logging into the AWS Console
1. Install the AWS CLI onto your computer
   * For instructions on installation follow go to the link in the references.
2. Run the command **aws configure** to connect to your AWS account 
3. You will be prompted for your AWS Access Key ID and AWS Secret Access Key. Enter both your AWS Access Key ID and AWS Secret Access Key
4. Now you should be in your AWS console

Launching a CloudFormation Template
1. Create an S3 bucket using the following command: aws s3 mb s3://**bucketname** --region **insertregioin**
   * Example of region foramt: us-east-1a
   * Adding the region is optional. If you choose not to specify the region it will create the bucket in the default region of you AWS account 
2. **cd** to the local directory where your template is located 
3. Upload your template into the S3 bucket using the following command: aws s3 cp **localfilename.yaml** s3://**bucketname**
4. Get the object URL for your template in the S3 console
5. Launch the stack in CloudFormation using the following command:
   * aws cloudformation create-stack --stack-name **stackname** --template-url **inserttemplateurl**
      * If your template doesn't have parameters that need to be filled 
   * aws cloudformation create-stack --stack-name **stackname** --template-url **inserttemplateurl** --parameters ParameterKey=**parameterkey**,ParameterValue=**parametervalue**
      * If your template has parameters that need to filled
6. If your template lauches successfully you will receive a "StackId" as an ouput in your terminal. 

      <details>
      <summary>Click Here</summary>

      ![CLI-Launch](https://gitlab.com/b.owoyele/images/-/raw/main/CLILaunch.png)

      </details>


#### **Objective 3:**

To create a nested stack do the following:

* Create an S3 Bucket. You will need to upload the nested templates into an S3 bucket because it is from this S3 bucket that CloudFormation will get the template to launch our environment
* Take our CloudFormation template from objective 1 and break it down into a main template (that can be thought of as an outline template) and nested templates (that can be thought of as a sub-templates that will launch individual resources).

Steps: 
1. Take the template in objective 1, change the resource section so that this template references  your nested templates 
2. In the parameters add a section where you can reference the S3 bucket where your nested templates will be located
    * See main-stack in the repository as an example
3. Create a nested template for each resource needed to create the infrastructure
    * See the following in the repository as an examples:
        * vpc-stack
        * publicsubnet-stack
        * privatesubnet-stack 
        * nat-privatert-stack
        * igw-publicrt-stack
        * sg-stack
4. Upload all of your nested templates to the created S3 Bucket
5. Return to the CloudFormation dashboard and upload your main-stack template
   * When you upload your main template you will notice that  a place for your S3 bucket in the parameters. Enter the name of the S3 bucket name that contains the nested templates

      <details>
      <summary>Click Here</summary>

      ![New_Note](https://gitlab.com/b.owoyele/images/-/raw/main/Parameters.jpg)

      </details>
6. Launch the template
7. The infrastructure will successfully launch

#### **Objective 4:**

1. Create an S3 bucket 
   * see objective 2 for instructions on how to create an s3 bucket in the CLI
2. Create a packaged template by performing the following:
   * **cd** to the local directory where your main/ root template is located
   * Run the command: aws cloudformation package --template-file **roottemplate.yaml** --output-template **packaged.yaml** --s3-bucket **s3bucketname**
3. Your will get the output packaged.yaml in your local directory. This template will have all of the necessary assets (nested stacks) uploaded from the development S3 bucket.
4. To deploy the nested stack run the following: aws cloudformation deploy --region **insertregion** --template-file **packaged.yaml** --stack-name **stackname** --parameter-overrides **parameter=value**
5. When your template launches you will receive a confirmation message in the terminal

#### **References**
* <https://cloudacademy.com/blog/how-to-use-aws-cli/>
